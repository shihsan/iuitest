//
//  UGViewControllerItem1.h
//  iuiTest
//
//  Created by Chung Yu Huang  on 2014/6/24.
//  Copyright (c) 2014年 bobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UGViewControllerItem1 : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) NSMutableArray *FIFAImages;
@end
