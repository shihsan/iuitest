//
//  UGViewControllerItem1.m
//  iuiTest
//
//  Created by Chung Yu Huang  on 2014/6/24.
//  Copyright (c) 2014年 bobo. All rights reserved.
//

#import "UGViewControllerItem1.h"
#import "UGMyCell.h"

@interface UGViewControllerItem1 ()

@end

@implementation UGViewControllerItem1

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _FIFAImages= [@[@"bra.png",
                       @"mex.png",
                       @"cro.png",
                       @"cmr.png",
                       
                       @"ned.png",
                       @"chi.png",
                       @"esp.png",
                       @"aus.png",
                       
                       @"col.png",
                       @"civ.png",
                       @"jpn.png",
                       @"gre.png",
                       
                       @"crc.png",
                       @"eng.png",
                       @"ita.png",
                       @"uru.png",
                       
                       @"fra.png",
                       @"ecu.png",
                       @"sui.png",
                       @"hon.png",
                       
                       @"arg.png",
                       @"nga.png",
                       @"irn.png",
                       @"bih.png",
                       
                       @"por.png",
                       @"gha.png",
                       @"ger.png",
                       @"usa.png",
                       
                    ] mutableCopy];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    return _FIFAImages.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UGMyCell *myCell = [collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"MyCell"
                                    forIndexPath:indexPath];

    UIImage *image;
    long row = [indexPath row];
    
    image = [UIImage imageNamed:_FIFAImages[row]];
    
    myCell.imageView.image = image;
    
    [myCell.label setText:_FIFAImages[row]];
    
    /* Border */
    [myCell.contentView.layer setBorderColor:[UIColor blackColor].CGColor];
    [myCell.contentView.layer setBorderWidth:3.0f];
    
    return myCell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;
    size.width = 100;
    size.height = 100;
    return size;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader) {
        
        UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        if (reusableview==nil) {
            reusableview=[[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        }
        
        UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        label.text=[NSString stringWithFormat:@"A"];
        [reusableview addSubview:label];
        return reusableview;
    }
    return nil;
}
//-(NSString *)collectionView:(UICollectionView *)collectionView titleForHeaderInSection:(NSInteger)section
//{
//    NSString *sectionName;
//    switch (section)
//    {
//        case 0:
//            sectionName = NSLocalizedString(@"A", @"A");
//            break;
//        case 1:
//            sectionName = NSLocalizedString(@"B", @"B");
//            break;
//        case 2:
//            sectionName = NSLocalizedString(@"C", @"C");
//            break;
//        case 3:
//            sectionName = NSLocalizedString(@"D", @"D");
//            break;
//        default:
//            sectionName = @"";
//            break;
//    }
//    return sectionName;
//}




@end
