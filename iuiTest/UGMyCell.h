//
//  UGMyCell.h
//  iuiTest
//
//  Created by Chung Yu Huang  on 2014/6/24.
//  Copyright (c) 2014年 bobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UGMyCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
